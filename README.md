# easy-toot

Most simple API to send a toot with text and media

## Secrets

An environment variable `MASTODON_SECRETS` or a file named `.mastodon-secrets.json` need to exist and contain the following:

```json
{
  "api_url": "https://mastodon.social/api/v1/",
  "access_token": "..."
}
```

### Text and media

A `payload.json` in the current directory can contain the following entries:

```json
{
  "spoilerText": "SPOILER!!!",
  "text": "Here comes the text.",
  "mediaUrls": ["https://example.com/som/image.png"]
}
```

The `spoilerText` and `mediaUrls` keys are optional.
