#!/usr/bin/env node

const fs = require('fs')
const https = require('https')
const Mastodon = require('mastodon')

const mastodonSecrets = JSON.parse(
  process.env.MASTODON_SECRETS || fs.readFileSync('.mastodon-secrets.json')
)

const mastodonClient = new Mastodon(mastodonSecrets)

const fetchMedia = url =>
  new Promise(resolve => https.get(url, file => resolve(file)))

const sendToot = (text, spoilerText, mediaIds) => {
  const params = {
    status: text,
    media_ids: mediaIds
  }

  if (spoilerText) {
    params.spoiler_text = spoilerText
  }

  return mastodonClient.post('statuses', params).then(({ data: toot }) => {
    console.log(toot.uri)
  })
}

const uploadMedia = url =>
  fetchMedia(url)
    .then(file => mastodonClient.post('media', { file }))
    .then(response => response.data.id)

const payload = JSON.parse(fs.readFileSync('payload.json'))
const { text, spoilerText, mediaUrls = [] } = payload
Promise.all(mediaUrls.map(uploadMedia))
  .then(mediaIds => sendToot(text, spoilerText, mediaIds))
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
